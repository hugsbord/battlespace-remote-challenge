/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   btsp.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hugsbord <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/06/02 22:23:15 by hugsbord          #+#    #+#             */
/*   Updated: 2020/06/02 23:34:55 by s72h             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "btsp.h"

char	*ft_shoot(int x, int y)
{
	static char pos[4];

	pos[0] = x + 'A';
	pos[1] = y + '0';
	pos[2] = '\n';
	return (pos);
}

int		ft_tri(int x, int y, t_lst *list)
{
	int		i;
	t_pos	*pos;

	i = 0;
	if (list == NULL)
		return (0);
	pos = list->first;
	while (pos != NULL)
	{
		if (x == pos->x && y == pos->y)
			i++;
		pos = pos->next;
	}
	if (i != 0)
		return (1);
	return (0);
}

int		ft_insert(int x, int y, t_lst *list)
{
	t_pos *new;

	new = malloc(sizeof(*new));
	if (list == NULL || new == NULL)
		return (0);
	new->x = x;
	new->y = y;
	new->next = list->first;
	list->first = new;
	return (0);
}

t_lst	*ft_init(void)
{
	t_lst	*list;
	t_pos	*elem;

	list = malloc(sizeof(*list));
	elem = malloc(sizeof(*elem));
	if (list == NULL || elem == NULL)
		return (NULL);
	elem->x = 0;
	elem->y = 0;
	elem->next = NULL;
	list->first = elem;
	return (list);
}

int		ft_solve(int i, int x, int y, t_lst *lst)
{
	char	buff[10];

	if (i < 1)
	{
		write(STDOUT_FILENO, ft_shoot(x, y), 3);
		read(STDIN_FILENO, buff, 10);
	}
	else if ((i > 0) && (ft_tri(x, y, lst) == 0))
	{
		write(STDOUT_FILENO, ft_shoot(x, y), 3);
		read(STDIN_FILENO, buff, 10);
	}
	if (buff[0] == 'H' || buff[0] == 'M')
		ft_insert(x, y, lst);
	return (0);
}
