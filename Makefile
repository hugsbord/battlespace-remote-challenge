# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: hugsbord <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/06/02 22:47:14 by hugsbord          #+#    #+#              #
#    Updated: 2020/06/02 23:34:27 by s72h             ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = btsp

INCLUDES = btsp.h

FLAGS = -Wall -Wextra -Werror

SRCS =	main.c		\
		btsp.c

OBJ = $(SRCS:.c=.o)

all: $(NAME)

$(NAME): $(SRCS)
	gcc $(CFLAG) -c $(SRCS)
	gcc $(OBJ)  -o $(NAME)

clean:
	@rm -f $(OBJ)

fclean: clean
	@rm -f $(NAME)

re: fclean all
