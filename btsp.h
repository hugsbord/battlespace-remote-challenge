/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   btsp.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hugsbord <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/06/02 22:31:24 by hugsbord          #+#    #+#             */
/*   Updated: 2020/06/02 23:35:26 by s72h             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef BTSP_H
# define BTSP_H
# include <stdlib.h>
# include <unistd.h>

typedef struct			s_pos
{
	int				x;
	int				y;
	struct s_pos	*next;
}						t_pos;

typedef struct			s_lst
{
	t_pos			*first;
}						t_lst;

t_lst					*ft_init(void);
int						ft_solve(int i, int x, int y, t_lst *lst);

#endif
