/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: s72h <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/06/02 23:04:54 by s72h              #+#    #+#             */
/*   Updated: 2020/06/02 23:05:09 by s72h             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "btsp.h"

int		main(void)
{
	int		i;
	int		x;
	int		y;
	t_lst	*lst;

	i = 0;
	lst = ft_init();
	while (i < 2)
	{
		x = 0;
		while (x < 10)
		{
			y = 0;
			while (y < 10)
			{
				ft_solve(i, x, y, lst);
				y++;
			}
			x++;
		}
		i++;
	}
	return (0);
}
